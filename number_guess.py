from random import randint

play_again = "y"

while play_again == "y":
  
  rand_num = randint(1,100)
  number = input("Enter a guess between 1 and 100: ")
  
  while True:
    
    try:
      val = int(number)
    except ValueError:
      print("Not a number")
    if number < rand_num:
      print("You guessed too low")
      number = input("Enter a guess between 1 and 100: ")
      
    elif number > rand_num:
      print("You guesed too high")
      number = input("Enter a guess between 1 and 100: ")
      
    else:
      print("You guessed right")
      play_again = raw_input("Would you like to play again?: ")
      break
